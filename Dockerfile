FROM node:latest

RUN mkdir -p /app/api_snap
WORKDIR /app/api_snap

ADD . /app/api_snap

ENV MONGO_URI mongodb://mongo/api_snap_db

COPY package.json /app/api_snap/
RUN npm install --quiet
COPY . /app
RUN ./node_modules/.bin/migrate up --autosync

EXPOSE 6088

CMD [ "npm", "start" ]
