const jwt = require('jsonwebtoken');
const passport = require('passport');
const mongoose = require('mongoose');
const validator = require('email-validator');

const User = mongoose.model('Users');

exports.registerUser = (req, res) => {
  if (!req.body.email || !req.body.password) {
    return res.status(400).json({
      code: 'E_MISSING_FIELD',
      message: 'Field email or password missing',
      status: 400,
      data: null,
    });
  }
  if (!validator.validate(req.body.email)) {
    return res.status(400).json({
      code: 'E_EMAIL_FORMAT_ERROR',
      message: 'Wrong email format',
      status: 400,
      data: null,
    });
  }
  User.findOne({ email: req.body.email }, (err, user) => {
    if (err) {
      return res.status(500).json({
        code: 'E_MONGODB_ERROR',
        message: err,
        status: 500,
        data: null,
      });
    }
    if (user) {
      return res.status(400).json({
        code: 'E_USER_ALREADY_EXIST',
        message: 'User with this email already exist',
        status: 400,
        data: null,
      });
    }

    const newUser = User(req.body);
    newUser.save((error, x) => {
      if (error) {
        return res.status(500).json({
          code: 'E_MONGODB_ERROR',
          message: error,
          status: 500,
          data: null,
        });
      }
      return res.json({
        code: 'S_REGISTERED',
        message: 'User registered',
        status: 200,
        data: { id: x._id, email: x.email },
      });
    });
    return null;
  });
  return null;
};

exports.loginStrategyAndCallback = async (req, res, next) => {
  if (!validator.validate(req.body.email)) {
    return res.status(400).json({
      code: 'E_EMAIL_FORMAT_ERROR',
      message: 'Wrong email format',
      status: 400,
      data: null,
    });
  }
  passport.authenticate('login', async (err, user, info) => {
    try {
      if (err || !user) {
        return res.status(400).json({
          code: 'E_LOGIN_MISS',
          message: info.message,
          status: 400,
          data: null,
        });
      }
      req.login(user, { session: false }, async (error) => {
        if (error) return next(error);
        const body = { _id: user._id, email: user.email };
        const token = jwt.sign({ user: body }, process.env.JWT_SECRET);
        return res.json({
          code: 'S_LOGGED',
          message: 'Successful login',
          status: 200,
          data: { token },
        });
      });
    } catch (error) {
      return next(error);
    }
    return null;
  })(req, res, next);
  return null;
};
