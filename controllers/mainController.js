exports.mainPage = (req, res) => {
  res.json({
    code: 'S_VIEW',
    message: 'This is the main page',
    status: 200,
    data: null,
  });
};

exports.testPage = (req, res) => {
  res.json({
    code: 'S_VIEW',
    message: 'This is the test page',
    status: 200,
    data: null,
  });
};
