const mongoose = require('mongoose');

const User = mongoose.model('Users');

module.exports = {
  getUsers: (req, res) => {
    User.find({}, (err, list) => {
      const x = [];
      list.forEach((element) => {
        x.push({ email: element.email });
      });
      return res.json({
        code: 'S_LIST_USERS',
        message: 'Users list',
        status: 200,
        data: x,
      });
    });
    return null;
  },

  getUserProfile: (req, res) => {
    User.findById(req.user._id, (err, currentUser) => {
      if (err) {
        return res.status(500).json({
          code: 'E_MONGODB_ERROR',
          message: err,
          status: 500,
          data: null,
        });
      }
      if (!currentUser) {
        return res.status(400).json({
          code: 'E_USER_ID_ERROR',
          message: err,
          status: 400,
          data: null,
        });
      }
      return res.json({
        code: 'S_PROFILE',
        message: 'User information',
        status: 200,
        data: {
          email: currentUser.email,
        },
      });
    });
  },
};
