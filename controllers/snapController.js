const mongoose = require("mongoose");
const Snap = mongoose.model("Snaps");
const fs = require("fs");
const multer = require("multer");
const jwt = require("jsonwebtoken");

exports.newSnap = (req, res) => {
  const Storage = multer.diskStorage({
    destination: function(req, file, callback) {
      callback(null, "./uploads");
    },
    filename: function(req, file, callback) {
      callback(
        null,
        file.fieldname + "_" + Date.now() + "_" + file.originalname
      );
    }
  });
  const user = jwt.verify(req.headers.token, process.env.JWT_SECRET).user;
  const upload = multer({
    storage: Storage
  }).array("image");
  upload(req, res, err => {
    if (err) {
      return res.status(401).json({
        message: err
      });
    }
    if (req.files == undefined || req.files.length == 0)
      return res.status(401).json({
        message: "File can't be empty"
      });
    console.log("File uploaded : " + req.files[0].filename);
    let newItem = new Snap();
    newItem.image.data = fs.readFileSync("./uploads/" + req.files[0].filename);
    newItem.image.contentType = "image/png";
    newItem.from = user.email;
    newItem.to = req.body.to;
    newItem.duration = req.body.duration;
    newItem.image.link = "/" + req.files[0].filename;
    newItem.save((err, result) => {
      if (err) {
        return res.status(400).json({
          code: "E_BUFFER",
          message: err,
          status: 400,
          data: null
        });
      }
      res.json({
        code: "S_SNAP",
        message: "Snap Content",
        status: 200,
        data: result
      });
    });
  });
};

exports.getSnap = (req, res) => {
  Snap.findOne({ _id: req.params.id }, (err, result) => {
    if (err) {
      return res.status(400).json({
        code: "E_BUFFER",
        message: err,
        status: 400,
        data: null
      });
    }
    res.json({
      code: "S_SNAP",
      message: "Snap Content",
      status: 200,
      data: result
    });
  });
};

exports.getSnaps = (req, res) => {
  const user = jwt.verify(req.headers.token, process.env.JWT_SECRET).user;
  Snap.find({ to: user.email }, (err, result) => {
    if (err) {
      return res.status(400).json({
        code: "E_BUFFER",
        message: err,
        status: 400,
        data: null
      });
    }

    var x = {
      code: "S_SNAPS",
      message: "Snap Content",
      status: 200,
      data: result
    };

    x.data.forEach(el => {
      el.image = null;
    });

    res.json(x);
  });
};

exports.deleteSnap = (req, res) => {
  if (!req.body.id) return res.send("Error");
  Snap.findOneAndDelete({ _id: req.body.id }, (err, result) => {
    if (!result) return res.send("Error");
    res.json({
      code: "S_DELETE_SNAP",
      message: "Snap Deleted",
      status: 200,
      data: "snap deleted"
    });
  });
};
