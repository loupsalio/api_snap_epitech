require("dotenv-safe").config();
const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const helmet = require("helmet");
const cors = require("cors");
const swaggerUi = require("swagger-ui-express");
const passport = require("passport");

const app = express();

// Setup express server port from ENV, default: 6088
app.set("port", process.env.PORT || 6088);

// Enable only in development HTTP request logger middleware
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

// for parsing json
app.use(
  bodyParser.json({
    limit: "20mb"
  })
);

// for parsing application/x-www-form-urlencoded
app.use(
  bodyParser.urlencoded({
    limit: "20mb",
    extended: true
  })
);

app.use(express.static(__dirname + "/uploads"));

// MongoDB configuration

require("./config/db").init_db();

// Init all other stuff
app.use(cors());
app.use(helmet());

// Init passport

require("./config/passport.js");

// Init routes

app.use("/", require("./routes/main"));
app.use("/", require("./routes/auth"));
app.use("/", require("./routes/snap"));
app.use(
  "/",
  passport.authenticate("jwt", { session: false }),
  require("./routes/profile")
);

const expressSwagger = require("express-swagger-generator")(app);

let options = {
  swaggerDefinition: {
    info: {
      description: "api_snap",
      title: "api_snap",
      version: "1.0.0"
    },
    host: "localhost:6088",
    basePath: "/v1",
    produces: ["application/json", "application/xml"],
    schemes: ["http", "https"],
    securityDefinitions: {
      JWT: {
        type: "bearer token",
        in: "header",
        name: "Authorization",
        description: ""
      }
    }
  },
  basedir: __dirname, // app absolute path
  files: ["./routes/*.js"] // Path to the API handle folder
};
expressSwagger(options);

// swagger documentation - Do not remove on prod / Remove on dev

// const swaggerDocument = require("./config/swagger");

// const options = {
//   swaggerOptions: {
//     validatorUrl: null
//   },
//   customCss: ".swagger-ui .topbar { display: none }",
//   customSiteTitle: "api_snap API"
// };

// app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));

// 404 errors

app.all("*", (req, res) =>
  res.status(404).json({
    code: "E_NOT_FOUND",
    message: `${req.originalUrl} path not found`,
    status: 404,
    data: null
  })
);
app.listen(
  app.get("port"),
  console.log("Listening on port : " + app.get("port"))
);

module.exports = app; // for testing
