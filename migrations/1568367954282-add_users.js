require("dotenv-safe").config();
require("../config/db").init_db();
const mongoose = require("mongoose");

const User = mongoose.model("Users");

/**
 * Make any changes you need to make to the database here
 */
async function up() {
  await new User({
    name: "Ada",
    email: "admin@admin.fr",
    active: true,
    role: "admin",
    password: "$2b$10$Blwk130wokguY49j8M04MONWJO5cmfxdUqVFjtMz4eIwR5xdJ9Ed." // pwd:starwolf
  }).save();
}

/**
 * Make any changes that UNDO the up function side effects here (if possible)
 */
async function down() {
  await User.deleteOne({
    name: "Ada"
  }); // Write migration here
}

module.exports = { up, down };
