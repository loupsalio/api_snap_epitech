const mongoose = require("mongoose");

const { Schema } = mongoose;

const SnapSchema = new Schema(
  {
    from: {
      type: String,
      lowercase: true,
      required: true
    },
    to: {
      type: String,
      lowercase: true,
      required: true
    },
    duration: {
      type: Number,
      default: 10
    },
    image: { data: Buffer, contentType: String, link: String }
  },
  {
    versionKey: false,
    timestamps: true
  }
);

module.exports = mongoose.model("Snaps", SnapSchema);
