const mongoose = require("mongoose");

const { Schema } = mongoose;
const bcrypt = require("bcrypt");

const UserSchema = new Schema(
  {
    email: {
      type: String,
      lowercase: true,
      unique: true,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    active: {
      type: Boolean,
      default: false
    },
    verification: {
      type: String,
      default: ""
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
);

UserSchema.pre("save", async function saveashedPassword(next) {
  const hash = await bcrypt.hash(this.password, 10);
  this.password = hash;
  next();
});

UserSchema.methods.checkPassword = async function checkPassword(password) {
  const user = this;
  const compare = bcrypt.compareSync(password, user.password, (err, result) => {
    if (err) {
      throw err;
    }
    throw new Error(result);
  });
  return compare;
};

module.exports = mongoose.model("Users", UserSchema);
