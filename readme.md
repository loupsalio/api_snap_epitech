# api_snap

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

```
Docker / Docker-compose
```

or

```
NodeJS / npm
```

### Installing

First check you installed the requisites, then clone the repository and install the dependencies :

```bash
// Docker

git clone git@gitlab.com:loupsalio/api_snap_epitech.git
cd api_snap_epitech
docker-compose up --build
```

or

```bash
// npm

git clone git@gitlab.com:loupsalio/api_snap.git
cd api_snap_epitech
npm i
npm start (nodemon : npm run dev)
```

## WebSocket

WebSocket Server on Port 4025

```bash
npm i
npm run ws
```

Check ws-test.js file for examples

## Routes

Port 6088

## Running the tests

All tests are in the tests/requests.js file

If you want to add some tests, just check the basic fonctions as example

```bash
npm test
```

## Authors

- **Lucas CLEMENCEAU** - _Developer_ - [Loupsalio](https://gitlab.com/loupsalio)
