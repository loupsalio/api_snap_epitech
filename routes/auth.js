const express = require('express')
const controller = require('../controllers/authController')

const router = express.Router()

/**
 * Register system
 * @route POST /inscription
 * @group Auth - Operations on the Authentification system
 * @param {string} name.body.required - user's name
 * @param {string} email.body.required - user's email
 * @param {string} password.body.required - user's password
 * @returns {object} 200 - Return the user registered
 * @returns {Error} 400 - Unauthorized => invalid parameter
 * @returns {Error} 500 - Unexpected error
 */
router.post('/inscription', controller.registerUser)

/**
 * Login system
 * @route POST /auth/login
 * @group Auth - Operations on the Authentification system
 * @param {string} email.body.required - user's mail
 * @param {string} password.body.required - user's password
 * @returns {object} 200 - Return the user JWT
 * @returns {Error} 400 - Unauthorized => invalid parameter
 * @returns {Error} 500 - Unexpected error
 */
router.post('/connection', controller.loginStrategyAndCallback)

module.exports = router
