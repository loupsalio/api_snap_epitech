const express = require('express');
const controller = require('../controllers/mainController');

const router = express.Router();

/**
 * Main page
 * @route GET /
 * @group Main - Operations on the view
 * @returns {object} 200 - Return JSON informations
 * @returns {Error} 500 error - Unexpected error
 */
router.route('/').all(controller.mainPage);

/**
 * Test page
 * @route GET /test
 * @group Main - Operations on the view
 * @returns {object} 200 - Return JSON example
 * @returns {Error} 500 error - Unexpected error
 */
router.route('/test').all(controller.testPage);
module.exports = router;
