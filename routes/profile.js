const passport = require('passport');
const express = require('express');
const controller = require('../controllers/profileController');

const router = express.Router();

/**
 * Get logged user informations
 * @route GET /profile/me
 * @group Profile - Operations on user entity
 * @returns {object} 200 - return json message object
 * @returns {Error}  error - Unexpected error
 */
router.get('/me', controller.getUserProfile);

router.get('/all', controller.getUsers);

module.exports = router;
