const express = require("express");
const controller = require("../controllers/snapController");

const router = express.Router();

router.post("/snap", controller.newSnap);
router.get("/snap/:id", controller.getSnap);
router.get("/snaps", controller.getSnaps);
router.post("/seen", controller.deleteSnap);

module.exports = router;
