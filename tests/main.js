/* eslint-disable no-param-reassign */
/* eslint-disable no-undef */

const supertest = require('supertest');
const mongoose = require('mongoose');
const app = require('../index');

const request = supertest.agent(app);
const User = mongoose.model('Users');

before((done) => {
  User.deleteMany({}, (err) => {
    if (err) {
      throw new Error(err);
    }
  }).then(() => {
    new User({
      name: 'test user',
      email: 'admin@admin.fr',
      password: 'starwolf',
    }).save(() => {
      new User({
        name: 'test user 2',
        email: 'test@admin.fr',
        password: 'starwolf',
      }).save(() => {
        done();
      });
    });
  });
});

describe('api_snap API', () => {
  // Main tests

  describe('Main', () => {
    describe('GET /', () => {
      it('should return default JSON message', async () => request
          .get('/')
          .expect('Content-Type', /json/)
          .expect(200, {
            code: 'S_VIEW',
            message: 'This is the main page',
            status: 200,
            data: null,
          }));
    });

    describe('GET /test', () => {
      it('should return test JSON message', async () => request
          .get('/test')
          .expect('Content-Type', /json/)
          .expect(200, {
            code: 'S_VIEW',
            message: 'This is the test page',
            status: 200,
            data: null,
          }));
    });
  });

  // Auth tests

  describe('Auth', () => {
    describe('POST /auth/register', () => {
      it('should return registered JSON message', () => request
          .post('/auth/register')
          .send({
            name: 'test',
            email: 'test@test.fr',
            password: 'starwolf',
          })
          .set('Accept', 'application/json')
          .expect((res) => {
            res.body.data.id = 'test';
          })
          .expect(200, {
            code: 'S_REGISTERED',
            message: 'User registered',
            status: 200,
            data: {
              id: 'test',
              name: 'test',
              email: 'test@test.fr',
            },
          }));
    });

    describe('POST /auth/login', () => {
      it('should return logged JSON message', () => request
          .post('/auth/login')
          .send({ email: 'admin@admin.fr', password: 'starwolf' })
          .set('Accept', 'application/json')
          .expect((res) => {
            res.body.data.token = 'test';
          })
          .expect(200, {
            code: 'S_LOGGED',
            message: 'Successful login',
            status: 200,
            data: {
              token: 'test',
            },
          }));
    });
  });

  // Profile tests
  let token = null;

  before((done) => {
    request
      .post('/auth/login')
      .send({ email: 'admin@admin.fr', password: 'starwolf' })
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        token = res.body.data.token; // Or something
        return done();
      });
  });

  describe('Profile', () => {
    describe('GET /profile/me', () => {
      it('should return user information JSON message', () => request
          .get('/profile/me')
          .set('Authorization', `bearer ${token}`)
          .set('Accept', 'application/json')
          .expect(200, {
            code: 'S_PROFILE',
            message: 'User information',
            status: 200,
            data: {
              name: 'test user',
              email: 'admin@admin.fr',
              role: 'user',
            },
          }));
    });
  });
});
